"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_lorenz.py
```
"""

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.lorenz.solver import Simul

from util import savefig

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.02
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs0 + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys0 - 1.0)
sim.state.state_phys.set_var("Z", sim.Zs0 - 10.0)

sim.time_stepping.start()

sim.output.print_stdout.plot_XY()
# see also the other plot_* methods of sim.output.print_stdout

# Note: to modify the figure and/or save it, you can use
ax = plt.gca()
fig = ax.figure
# use `ax`` and `fig`` to modify the figure as needed
...
savefig(fig, f"{sim.name_run}_plot_XY.png")

print(
    f"""
This simulation can be reloaded in IPython by running (in the terminal):
fluidsim-ipy-load {sim.output.path_run}
"""
)

plt.show()
